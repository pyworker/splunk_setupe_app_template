
# Именование endpoint c помощью restmap.conf

```
[admin_external:customendpoint]
handlertype = python
handlerfile = MyApp_python_handler.py
handleractions = list, edit
```

где 
```
[admin_external:<endpoint_name>]: название endpoint
handlertype: задает язык программирования для скрипта, реализующего REST endpoint;
handlerfile: имя скрипта python в <app_name>/bin/
handleractions: REST действия, поддерживаемые скриптом. list принимает начальные значения полей и отображает их на экране настройки. edit обрабатывает значения POSTED из сохраненного экрана настройки.
```

Пимер заполнения полей
```
[admin_external:alert_manager]
handlertype = python
handlerfile = alert_manager_config.py
handleractions = list, edit
```

Установленные в `restmap.conf` настройки настраивает endpoint на `.../admin/<endpoint_name>`

# Реализация endpoint


Пример alert_manager.conf
```
[settings]
incident_list_length = 20
index = alerts
default_owner = unassigned
default_impact = low
default_urgency = low
default_priority = low
user_directories = both
```



1. Указать набор полей, котрые требуется задать пользователю в файле `default/ot_config.conf`
2. В `default/setupe.xml` задать форму для заполнения параметров
```
<setup>
    <block title="<section_name>" endpoint="admin/ot_endpoint" entity="settings">
        <text>Config for user</text>
        <input field="<field_name>">
            <label>field description</label>
            <type>field_type</type>
        </input>
</setup>
```
3. Добавить поля в `bin/ot_app_config.py` 


Результат выполнения rest-запроса
```
{
    'ot_password_field': '1111',
    'eai:attributes': {
        'requiredFields': [],
        'optionalFields': [
            'ot_boolean_field',
            'ot_list_field',
            'ot_password_field',
            'ot_text_field_1',
            'ot_text_field_2',
            'ot_text_username'
        ],
        'wildcardFields': []
    },
    'ot_text_field_1': 'test_alert',
    'ot_boolean_field': '0',
    'ot_text_field_2': '8989',
    'ot_text_username': 'admin',
    'ot_list_field': 'name, sername, tttttt',
    'eai:acl': {
        'can_write': '1',
        'removable': '0',
        'owner': 'system',
        'can_list': '1',
        'sharing': 'system',
        'modifiable': '0',
        'app': None,
        'perms': {
            'read': ['*'],
            'write': ['*']
        }
    }
}
```
