"""
module for obtaining application settings using REST
and test setup function
"""
import splunk.entity
from utils import get_splunk_session_key

# set MAIN PARAMETERS
APP_NAME = 'ot_app_setupe'
ENDPOINT_NAME = '/admin/ot_endpoint'


def get_app_settings(session_key):
    """
    get aplication settings by rest
    application settings are specified in the configuration file "local/ot_conf.conf"
    or in "default/ot_conf.conf"
    Args:
        session_key: str, SPlunk session key
    Raises:
        Exception: resource not found
    Return:
        application settings from Splunk server
    """
    try:
        app_settings = splunk.entity.getEntity(
                            ENDPOINT_NAME,
                            'settings',
                            namespace=APP_NAME,
                            sessionKey=session_key,
                            owner='nobody'
                        )

    except splunk.ResourceNotFound as not_found:
        raise Exception('failed to get app settings: resource not found %s' % not_found.message)
    return app_settings

if __name__ == '__main__':
    session_key = get_splunk_session_key()
    settings = get_app_settings(session_key=session_key)
    print(settings)
