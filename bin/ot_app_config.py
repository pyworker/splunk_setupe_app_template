
import splunk.admin as admin
import splunk.entity as entity
    

# set up app setup file
# example ot_config.conf
APP_CONFIG = 'ot_config'


class ConfigApp(admin.MConfigHandler):
    def setup(self):
        """
        set up supported arguments
        set the list of fields that should be displayed to the user for configuration
        """
        if self.requestedAction == admin.ACTION_EDIT:

            # all fields listed in "default/<APP_CONFIG.conf>"
            for arg in [
                        'ot_text_field_1',
                        'ot_text_field_2',
                        'ot_boolean_field',
                        'ot_list_field',
                        'ot_password_field',
                        'ot_text_username'
                    ]:
                self.supportedArgs.addOptArg(arg)
        pass

    def handleList(self, confInfo):
        """
        read the initial values of the parameters from the custom
        file myappsetup.conf and write them to the setup screen
        
        search for a configuration file in:
            <appname>/default/<app_setupe>.conf if the app has never been set up 
            <appname>/local/<app_setupe>.conf if app has been set up, and looks at
                <appname>/default/<app_setupe>.conf if there is no value for a field
                in <appname>/local/myappsetup.conf

        for boolean fields, may need to switch the true/false setting
        for text fields, if the conf file says None, set to the empty string.
        """
        confDict = self.readConf(APP_CONFIG)
        if None != confDict:
            for stanza, settings in confDict.items():
                for key, val in settings.items():

                    #
                    # work with text field in config
                    #

                    if key in ['ot_text_field_1'] and val in [None, '']:
                        val = ''    
                    if key in ['ot_text_field_2'] and val in [None, '']:
                        val = ''
                    if key in ['ot_text_username'] and val in [None, '']:
                        val = ''
                    
                    #
                    # work with boolean field in config
                    #
                    if key in ['ot_boolean_field'] and val in [None, '']:
                        if int(val) == 1:
                            val = '0'
                        else:
                            val = '1'

                    confInfo[stanza].append(key, val)

    def handleEdit(self, confInfo):
        """
        after user clicks Save on setup screen, take updated parameters, 
        normalize them, and save them
        """
        name = self.callerArgs.id
        args = self.callerArgs

        #
        # work with text field in config
        #
        if self.callerArgs.data['ot_text_field_1'][0] in [None, '']:
            self.callerArgs.data['ot_text_field_1'][0] = ''

        if self.callerArgs.data['ot_text_field_2'][0] in [None, '']:
            self.callerArgs.data['ot_text_field_2'][0] = ''

        if self.callerArgs.data['ot_text_username'][0] in [None, '']:
            self.callerArgs.data['ot_text_username'][0] = ''

        #
        # work with boolean field in config
        #
        if int(self.callerArgs.data['ot_boolean_field'][0]) == 1:
            self.callerArgs.data['ot_boolean_field'][0] = '0'
        else:
            self.callerArgs.data['ot_boolean_field'][0] = '1'


        #
        # "settings": its a name of section in conf file in local file
        #
        self.writeConf(APP_CONFIG, 'settings', self.callerArgs.data)                        
                    
# initialize the handler
admin.init(ConfigApp, admin.CONTEXT_APP_AND_USER)


